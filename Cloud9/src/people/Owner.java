/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package people;

/**
 *
 * @author Jono
 */
import enums.eDepartment;
public class Owner extends Admin{
    // The owner is an admin who should have more rights than
    // other admins
    
    // This constructor creates an Owner with all the needed information. Note that the department is null as the owner
    // should have control over everything.
    public Owner(int hours, double pay, String firstname, String lastname, String ID, String password) {
       super(hours, pay, firstname, lastname, ID, password, eDepartment.NULL);
    }
    
    // This constructor creates a generic owner when not all information about the owner is known (i.e. at startup)
    public Owner(){
        super(0, 0, "John", "Doe", "0", "changethis", eDepartment.NULL);
    }
    
}
