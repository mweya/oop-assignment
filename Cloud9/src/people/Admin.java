/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package people;

/**
 *
 * @author Jono
 */
// Import the enums that specify the department the admin can be a part of here
import enums.eDepartment;
// The admin is an employee (polymorphism)
public class Admin extends Employee{
    
    private String firstname;
    private String lastname;
    private int ID;
    private String password;
    
    // Full constructor helps us create an admin all relevant information
    public Admin(int hours, double pay, String firstname, String lastname, String ID, String password, eDepartment department) {
        super(hours, pay, firstname, lastname, ID, password, department);
    }

    public Admin(String firstname, String lastname,int ID,String password) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.ID = ID;
        this.password = password;
        
    }
    
    public boolean authenticate(String password) {
        return super.authenticate(password);
    }
    
    public String getID() {
        return super.getID();
    }
    
    // Empty constructor creates a generic admin (for temporary use only)
   public Admin() {
       super(0, 0, "John", "Doe", "None", "changethis", eDepartment.NULL);
   } 
    
    
}
