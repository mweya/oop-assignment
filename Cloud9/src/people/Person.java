/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package people;

/**
 *
 * @author mweya
 */
// Abstract class to force devs to specify what kind of a person they are adding
public abstract class Person {
    // This is an example of encapsulation
    private static String firstname;
    private static String lastname;
    public static String ID;
    
    // This empty constructor is used when creating a generic person
    public Person() {}
    
    // This constructor is used to create a person with an ID
    public Person(String firstname, String lastname, String ID) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.ID = ID;
    }
    
    // This constructor is used to create a person without an ID
        public Person(String firstname, String lastname) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.ID = "None";
    }

    
        
   
    /**
     * @return the firstname
     */
    // This returns the first name of the person
    public static String getFirstname() {
        return firstname;
    }

    /**
     * @param aFirstname the firstname to set
     */
    // This changes the first name of this person
    public void setFirstname(String aFirstname) {
        firstname = aFirstname;
    }

    /**
     * @return the lastname
     */
    // This returns the last name of the person
    public static String getLastname() {
        return lastname;
    }

    /**
     * @param aLastname the lastname to set
     */
    // this sets the last name of the person
    public static void setLastname(String aLastname) {
        lastname = aLastname;
    }

    /**
     * @return the ID
     */
    // This gets the ID of the person
    public String getID() {
        return ID;
    }

    /**
     * @param aID the ID to set
     */
    // This sets the ID of the person
    public static void setID(String aID) {
        ID = aID;
    }
    
    // This method writes the contents of the object to a string which
    // is then returned to whatever called this
    public String toString() {
    return firstname+" "+lastname+"\n"+"- "+ID;
    
    }
    
    
}

