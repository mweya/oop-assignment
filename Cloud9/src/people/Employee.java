/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package people;

// Import the enum that specifies the department this employee is a part of
import enums.eDepartment;

/**
 *
 * @author Jono
 */
public class Employee extends Person implements EmployeeInterface{
    
    
    private int hours; //For calculating rate based on status and hours.
    private double rate; 
    private String password; // For logging in
    private eDepartment department; // For keeping track of who can access what

    
    //Constructor for employee with Person as super.
    // Generates an Employee object with all the information needed
    public Employee(int hours, double rate, String firstname, String lastname, String ID, String password, eDepartment department) {
        super(firstname, lastname, ID);
        this.hours = hours;
        this.rate = rate; // Per hour
        this.password = password;
        this.department = department; 
        
    }
    
    // Generates an employee (who is a person, hence super) with a first and last name
    public Employee(String firstname, String lastname) {
        super(firstname, lastname);
    }
    
    
    // An empty constructor to create a generic employee
    public Employee() {}
    
    // Returns the full name that an employee has
    public String getName() {
        return super.getFirstname()+" "+super.getLastname();
    }
    
    //Getters and Setters
    // Returns the amount of hours worked
    public int getHours() {
        return hours;
    }

    // Sets the amount of hours worked
    public void setHours(int hours) {
        this.hours = hours;
    }

    // Returns the amount of money this person earns per time period
    public double getPay() {
        return rate;
    }
    
    public String getID() {
        return super.getID();
    }
    
    // Sets the rate of the person
    public void setPay(double rate) {
        this.rate = rate;
    }
    
    // Calculates and returns the amount of money this person is owed
    public double calculateSalary() {
        return this.hours*this.rate;
    }
    
    // Tests to see if the password supplied matches the one stored
    // to facilitate authentication by password
    public boolean authenticate(String password) {
        if (password.equals(this.password)){
            return true;
        } else {
            return false;
        }
    }
    
}
