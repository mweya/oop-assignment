/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package people;

/**
 *
 * @author Jono
 */
public class Customer extends Person {
    
    private int rewardPoints;  //To get discounts 
    private double moneySpent; //To determine the amount of reward points to give
    private int activeDiscount; //stores the magnitude of the discount (as a percentage)
    public int freeVape = 0;
    
    // NB! Increment the points first and then look at the active discount!
    
    // This constructor creates a customer with all relevant information needed
    public Customer(int rewardPoints, double moneySpent, String firstname, String lastname) {
        super(firstname, lastname);
        this.rewardPoints = rewardPoints;
        this.moneySpent = moneySpent;
        this.activeDiscount = 0;
    }
    
    // Creates a generic customer who hqw no reward points as a punishment for not
    // signing up properly
    public Customer() {
        this.rewardPoints = 0;
        this.moneySpent = 0;
        this.activeDiscount = 0;
    }

    // Increments this customer's reward points by the amount specified
    public void incRewardPoint(int amount) {
        this.rewardPoints = this.rewardPoints+amount;
    }
   
    // Sets the active discount the customer experiences to the correct amount depending on how many
    // points he or she has.
    public void setDiscount() {
        if (this.rewardPoints < 10) {
            this.activeDiscount = 0;
        } else if (this.rewardPoints < 15) {
            this.activeDiscount = 5;
        } else if (this.rewardPoints < 20) {
          this.activeDiscount = 7;
        } else {
            this.activeDiscount = 10;
        }
        
        if (this.rewardPoints > 99) {
            this.freeVape = 1;
        }
    }
    
    //Getters and setters
    public int getRewardPoints() {
        return rewardPoints;
    }

    public void setRewardPoints(int rewardPoints) {
        this.rewardPoints = rewardPoints;
    }

    public double getMoneySpent() {
        return moneySpent;
    }

    public void setMoneySpent(double moneySpent) {
        this.moneySpent = moneySpent;
    }
    
    
    @Override
    public String toString() {
        return super.getFirstname()+" "+super.getLastname()+"\n"+rewardPoints+" reward points\n"+activeDiscount+"% discount on your purchase";
    }
    
    
    
}
