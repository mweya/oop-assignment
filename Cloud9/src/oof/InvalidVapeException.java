package oof;

/*
 * Copyright 2018 mweya.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 *
 * @author mweya
 */
// This is a custom error message to be thrown when attempting to create a vape with parts
// that do not match
public class InvalidVapeException extends Exception{
    // If no information is given, return a generic string
    public InvalidVapeException() {
        super("These mods cannot be combined.");
    }
    // If information about the error is given, tell the user what went wrong
    public InvalidVapeException(String msg) {
        super(msg);
    }
}
