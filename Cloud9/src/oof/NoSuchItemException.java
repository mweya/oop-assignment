/*
 * Copyright 2018 mweya.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package oof;

/**
 *
 * @author mweya
 */
// This error message should be thrown when an item that does not exist is requested
public class NoSuchItemException extends Exception{
    // If no information is given, return a generic error message
    public NoSuchItemException() {
        super("The item specified does not exist.");
    }
    // If information about the error is given, tell the user what went wrong
    public NoSuchItemException(String msg) {
        super(msg);
    }
}
