/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package enums;

/**
 *
 * @author mweya
 */
// This file contains a list of acceptable colors
public enum eColor {
    WHITE,
    BLACK,
    BLUE,
    RED,
    GREEN,
    PURPLE,
    CYAN,
    YELLOW,
    CUSTOM
}
