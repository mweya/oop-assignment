/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package enums;

/**
 *
 * @author mweya
 */
// This contains a list of wire types to be used in coils
public enum eWire {
    KANTHAL32,
    KANTHAL30,
    KANTHAL28,
    KANTHAL26,
    KANTHAL24,
    KANTHAL22,
    NICHROME32,
    NICHROME30,
    NICHROME28,
    NICHROME26,
    NICHROME24,
    NICHROME22,
    STEEL32,
    STEEL30,
    STEEL28,
    STEEL26,
    STEEL24,
    STEEL22,
    NICKEL32,
    NICKEL30,
    NICKEL28,
    NICKEL26,
    NICKEL24,
    NICKEL22,
    TITANIUM32,
    TITANIUM30,
    TITANIUM28,
    TITANIUM26,
    TITANIUM24,
    TITANIUM22
}
