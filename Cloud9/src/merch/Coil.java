/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package merch;

/**
 *
 * @author mweya
 */
// Import a list of appropriate wires that this can be made from
import enums.eWire;
// This coil is an item and should be accessed using the ModInterface interface
public class Coil extends Item implements ModInterface {
    // Information about this coil goes here
    private double resistance;
    private eWire wire;

    // Generic constructor for construction generic objects
    public Coil() {}
    
    // full constructor based on the information given
    public Coil(eWire wire) {
        this.wire = wire;
        if (wire.name().startsWith("22")){
            this.resistance=0.2;
        } else {
            this.resistance = 0.5;
        }
    }

    // Getters and setters
    /**
     * @return the resistance
     */
    public double getResistance() {
        return resistance;
    }

    /**
     * @param resistance the resistance to set
     */
    public void setResistance(double resistance) {
        this.resistance = resistance;
    }

    /**
     * @return the wire
     */
    public eWire getWire() {
        return wire;
    }

    /**
     * @param wire the wire to set
     */
    public void setWire(eWire wire) {
        this.wire = wire;
    }
    
    // Forces the program to return information about the coil in an orderly fashion
    @Override
    public String toString() {
        return "Coil properties:\n"+wire+" wire with "+resistance+"Ω of resistance\n";
    }
    
}
