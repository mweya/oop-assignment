/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package merch;

/**
 *
 * @author mweya
 */
// Imports a list of acceptable form factor types
import enums.eForm;
// This is an item that should be interfaced with using the appropriate interface
public class Tank extends Item implements ModInterface {
    public double capacity; // the size of the tank in ml
    public eForm form;// The size of vape this tank is suited for
    
    // Generic empty constructor
    public Tank() {}
    
    // Creates a tank with all the relevant information
    public Tank(double capacity, eForm form) {
        this.capacity = capacity;
        this.form = form;
    }
    
    // Forces the system to output information about it in a formatted way
    @Override
    public String toString(){
        return "Tank Properties:\n"+capacity+"ml large\nWill fit in a "+form+" vape.\n";
    }
}
