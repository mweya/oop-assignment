/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package merch;

/**
 *
 * @author mweya
 */
// Get a list of appropriate form factoes
import enums.eForm;
// This is an item that should be used via the ModInterface
public class Battery extends Item implements ModInterface {
    private eForm form; // Stores information about the form factor that this battery should work with
    private double capacity; //in milliamp hours?
    
    // Empty constructor creates a generic battery
    public Battery() {}
    
    // Full constructor makes a battery object with all the relevant information
    public Battery(eForm form, double capacity) {
     this.form = form;
     this.capacity = capacity;
    }

    // Getters and setters
    /**
     * @return the form
     */
    public eForm getForm() {
        return form;
    }

    /**
     * @param form the form to set
     */
    public void setForm(eForm form) {
        this.form = form;
    }

    /**
     * @return the capacity
     */
    public double getCapacity() {
        return capacity;
    }

    /**
     * @param capacity the capacity to set
     */
    public void setCapacity(double capacity) {
        this.capacity = capacity;
    }
    
    // Forces the system to return information about this battery in an orderly fashion
    @Override
    public String toString() {
        return "Battery properties:\n"+capacity+"mAh large\n Will fit in a "+form+" vape.\n";
    }
}
