/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package merch;

/**
 *
 * @author mweya
 */
// This class describes the kind of liquid you can buy for your vapes
public class Liquid extends Item {
    // Information about the liquid goes here
    private String name;
    private double nicotineContent;
    
    // Generic and empty constructor for creating generic objects
    public Liquid() {}
    
    // Creates an object with all the relevant information
    public Liquid(String name, double nicotineContent) {
        this.nicotineContent = nicotineContent;
        this.name = name;
    }
    
    // Getters and setters
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the nicotineContent
     */
    public double getNicotineContent() {
        return nicotineContent;
    }

    /**
     * @param nicotineContent the nicotineContent to set
     */
    public void setNicotineContent(double nicotineContent) {
        this.nicotineContent = nicotineContent;
    }
    
    
}
