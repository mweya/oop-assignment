/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package merch;

/**
 *
 * @author mweya
 */
// Generic class for items, to be extended by more specific classes later
public class Item {
    // information about the items go here
    private String name;
    // Might not be needed, could just use position in array
    //public String id;
    private double price;
    
    // Generic constructor for creating an object without a lot of informaiton
    public Item() {}
    
    // Full constructor for creating an object with all the relevant information present
    public Item(String name, double price) {
        this.name = name;
        this.price = price;
    }

    // Getters and setters
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the price
     */
    public double getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(double price) {
        this.price = price;
    }
}
