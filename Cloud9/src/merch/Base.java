/*
 * Copyright 2018 mweya.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package merch;

/**
 *
 * @author mweya
 */
// This refers to the part that the mods are added to.
// feel free to rename this if there's a better name

// Import
import enums.eForm;
import enums.eColor;
public class Base extends Item implements ModInterface {
    private eForm form;
    private eColor color;
    
    public Base() {}
    public Base(eForm form, eColor color) {
        this.form = form;
        this.color = color;
    }

    /**
     * @return the form
     */
    public eForm getForm() {
        return form;
    }

    /**
     * @param form the form to set
     */
    public void setForm(eForm form) {
        this.form = form;
    }

    /**
     * @return the color
     */
    public eColor getColor() {
        return color;
    }

    /**
     * @param color the color to set
     */
    public void setColor(eColor color) {
        this.color = color;
    }
    
    @Override
    public String toString() {
        return "Base details:\nThis is a "+color+" "+form+" base.";
    }
}
