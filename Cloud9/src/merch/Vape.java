/*
 * Copyright 2018 mweya.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package merch;

/**
 *
 * @author mweya
 */
// This class is for the vape as a whole, including the items it's made of
public class Vape extends Item {
    // This is an example of composition as the vape consists of the coil, tank, battery and base
    private Coil coil; // The coil used
    private Tank tank; // the tank used
    private Battery battery; // the battery used
    private Base base; // the base used
    
    // Generic and empty constructor
    public Vape() {}
    
    // Full constructor to create a vape
    public Vape(Coil coil, Tank tank, Battery battery, Base base) {
        this.coil = coil;
        this.tank = tank;
        this.battery = battery;
        this.base = base;
        // Test the mods to make sure they'll fit here
    }

    /**
     * @return the coil
     */
    // Returns the coil object to whatever requested it
    public Coil getCoil() {
        return coil;
    }

    /**
     * @param coil the coil to set
     */
    // Sets the coil used in this vape to a specific kind
    public void setCoil(Coil coil) {
        this.coil = coil;
    }

    /**
     * @return the tank
     */
    // Returns the kind of tank used in this vape
    public Tank getTank() {
        return tank;
    }

    /**
     * @param tank the tank to set
     */
    // Sets the tank used in this vape to a specific one as specified
    public void setTank(Tank tank) {
        this.tank = tank;
    }

    /**
     * @return the battery
     */
    // Returns the battery used in this vape as an object
    public Battery getBattery() {
        return battery;
    }

    /**
     * @param battery the battery to set
     */
    // Sets the battery used in this vape to the one specified
    public void setBattery(Battery battery) {
        this.battery = battery;
    }

    /**
     * @return the base
     */
    // Returns the base used in this vape as an object
    public Base getBase() {
        return base;
    }

    /**
     * @param base the base to set
     */
    // Sets the base used in this vape to the one specified
    public void setBase(Base base) {
        this.base = base;
    }
    
}
