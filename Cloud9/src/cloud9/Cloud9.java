/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cloud9;

// This is the driver class. It is named after the store.

/**
 *
 * @author mweya
 */
import java.util.ArrayList;
import cloud9.*;
import enums.*;
import merch.Item;
import people.*;
import java.util.Scanner;
import merch.Base;
import merch.Battery;
import merch.Coil;
import merch.Liquid;
import oof.*;
public class Cloud9 {

    
    
 
 
 static Person user = null;
 static String storename = "Cloud9";
 static String motto = "The best in smoke.";
 //ArrayLists for different user accounts
 // This is an example of aggregation
 static ArrayList < Employee > owners = new ArrayList < Employee > ();
 static ArrayList < Employee > admins = new ArrayList < Employee > ();
 static ArrayList < Employee > employees = new ArrayList < Employee > ();
 static Scanner input = new Scanner(System.in);
 //ArrayList for item list
 static ItemList < Item > items = new ItemList < Item > ();
 static String[] stringstock = new String[]{"Base","Battery","Coil","Liquid","Tank"};
 static int[] intstock = new int[]{44,27,160,46,59};
 
 public static Person makeGenericAccount(String type) {
  if (type.equals("owner")) {
   people.Owner o = new people.Owner();
   return o;
  } else if (type.equals("employee")) {
   people.Employee e = new people.Employee();
   return e;
  } else if (type.equals("admin")) {
   people.Admin a = new people.Admin();
   return a;
  } else {
   people.Customer c = new people.Customer();
   return c;
  }
 }


 public static void makeVape() throws oof.InvalidVapeException {
  // Todo add code to make a vape here
 }

 public static void main(String[] args) {

  System.out.println(storename);
  System.out.println(motto + "\nCopyright Cloud9 inc. 2018");
  // Make the account for the owner and log them in
  user = makeGenericAccount("owner");
  menu();

 }

 
 static eColor itemColor = eColor.BLACK;
 static eForm itemForm = eForm.DESCREET;
 static eWire itemWire = eWire.KANTHAL22;
 static double totalAmount;
 static double itemCost;
 static int itemAmount;
 static double itemMath;
 
 
 public static void menu() {
  if (user == null) {
   System.out.println("\n\n1) Login as owner");
   System.out.println("2) Login as admin");
   System.out.println("3) Login as employee");
   System.out.println("4) Shut down");
   String s = input.next();
   if (s.equals("1")) {
    s = "";
    System.out.println("ID: ");
    String id = input.next();
    System.out.println("Password: ");
    String pass = input.next();
    login("owner", id, pass);
   } else if (s.equals("2")) {
    s = "";
    System.out.println("ID: ");
    String id = input.next();
    System.out.println("Password: ");
    String pass = input.next();
    login("admin", id, pass);
   } else if (s.equals("3")) {
    s = "";
    System.out.println("ID: ");
    String id = input.next();
    System.out.println("Password: ");
    String pass = input.next();
    login("employee", id, pass);
   }
   if (s.equals("4")) {
    System.exit(0);
   }
  } else {
   genMenu(user);
   if (user instanceof Owner) {
    String s = input.next();
    // Else not needed, menu will show if it breaks

    if (s.equals("1")) {
     System.out.println("\n\n1) Remove account");
     System.out.println("2) Change name");
     System.out.println("3) Change ID");
     System.out.println("4) Add account");
     System.out.println("5) To main menu");
     s = input.next();

     if (s.equals("1")) {

      try {
       System.out.println("User ID: ");
       String id = input.next();
       modOwner("r", id, "", "", "");
      } catch (Exception e) {
       System.out.println(e.toString());
      }
     } else if (s.equals("2")) {
      System.out.println("User ID: ");
      String id = input.next();
      System.out.println("\n\nFirstname:");
      String fname = input.next();
      System.out.println("Surname:");
      String sname = input.next();
      try {
       modOwner("cn", id, fname, sname, id);
      } catch (Exception e) {
       System.out.println(e.toString());
      }
     } else if (s.equals("3")) {
      System.out.println("User ID: ");
      String id = input.next();
      System.out.println("New ID: ");
      String nid = input.next();
      try {
       modOwner("ci", id, "", "", nid);
      } catch (Exception e) {
       System.out.println(e.toString());
      }
     } else if (s.equals("4")) {
      System.out.println("Firstname:");
      String fname = input.next();
      System.out.println("Lastname:");
      String lname = input.next();
      System.out.println("ID:");
      int id = input.nextInt();
      System.out.println("Password:");
      String pass = input.next();
      admins.add(new Admin(fname, lname, id, pass));
     }
    } else if (s.equals("2")) {
     System.out.println("\n\n1) Remove account");
     System.out.println("2) Change name");
     System.out.println("3) Change ID");
     System.out.println("4) Add account");
     System.out.println("5) To main menu");

     s = input.next();
     if (s.equals("1")) {


      if (s.equals("1")) {
       try {
        System.out.println("User ID: ");
        String id = input.next();
        modAdmin("r", id, "", "", "");
       } catch (Exception e) {
        System.out.println(e.toString());
       }
      } else if (s.equals("2")) {
       System.out.println("User ID: ");
       String id = input.next();
       System.out.println("\n\nFirstname:");
       String fname = input.next();
       System.out.println("Surname:");
       String sname = input.next();
       try {
        modAdmin("cn", id, fname, sname, id);
       } catch (Exception e) {
        System.out.println(e.toString());
       }
      } else if (s.equals("3")) {
       System.out.println("User ID: ");
       String id = input.next();
       System.out.println("New ID: ");
       String nid = input.next();
       try {
        modAdmin("ci", id, "", "", nid);
       } catch (Exception e) {
        System.out.println(e.toString());
       }
      } else if (s.equals("4")) {
       System.out.println("Firstname:");
       String fname = input.next();
       System.out.println("Lastname:");
       String lname = input.next();
       System.out.println("ID:");
       int id = input.nextInt();
       System.out.println("Password:");
       String pass = input.next();
       admins.add(new Admin(fname, lname, id, pass));
      }
     }
    } else if (s.equals("3")) {
     System.out.println("\n\n1) Remove account");
     System.out.println("2) Change name");
     System.out.println("3) Change ID");
     System.out.println("4) Add account");
     System.out.println("5) To main menu");
     s = input.next();
     if (s.equals("1")) {
      if (s.equals("1")) {
       try {
        System.out.println("User ID: ");
        String id = input.next();
        modEmployee("r", id, "", "", "");
       } catch (Exception e) {
        System.out.println(e.toString());
       }
      } else if (s.equals("2")) {
       System.out.println("User ID: ");
       String id = input.next();
       System.out.println("\n\nFirstname:");
       String fname = input.next();
       System.out.println("Surname:");
       String sname = input.next();
       try {
        modEmployee("cn", id, fname, sname, id);
       } catch (Exception e) {
        System.out.println(e.toString());
       }
      } else if (s.equals("3")) {
       System.out.println("User ID: ");
       String id = input.next();
       System.out.println("New ID: ");
       String nid = input.next();
       try {
        modEmployee("ci", id, "", "", nid);
       } catch (Exception e) {
        System.out.println(e.toString());
       }
      } else if (s.equals("4")) {
       System.out.println("Firstname:");
       String fname = input.next();
       System.out.println("Lastname:");
       String lname = input.next();
       System.out.println("ID:");
       int id = input.nextInt();
       System.out.println("Password:");
       String pass = input.next();
       admins.add(new Admin(fname, lname, id, pass));
      }
     }
    } else if (s.equals("4")) {System.out.println("1) Add Stock");
        System.out.println("2) Remove Stock");
        System.out.println("3) List Stock");
        s = input.next();
        if (s.equals("1")){
            System.out.println("Please choose item category to add");
            System.out.println("1)Base\n2)Battery\n3)Coil\n4)Liquid\n5)Tank");
            int stockResponse;
            stockResponse = input.nextInt();
            if(stockResponse == 1){
                System.out.println("How many bases do you want to add?");
                int baseAdd;
                baseAdd = input.nextInt();
                intstock[0] = intstock[0] + baseAdd;
            }else if(stockResponse == 2){
                System.out.println("How many batteries do you want to add?");
                int battAdd;
                battAdd = input.nextInt();
                intstock[1] = intstock[1] + battAdd;
            }else if (stockResponse == 3){
                System.out.println("How many coils do you want to add?");
                int coilAdd;
                coilAdd = input.nextInt();
                intstock[2] = intstock [2] + coilAdd;
            }else if (stockResponse == 4){
                System.out.println("How many liquids do you want to add?");
                int liquidAdd;
                liquidAdd = input.nextInt();
                intstock[3] = intstock [3] + liquidAdd;
            }else if (stockResponse == 5){
                System.out.println("How many tanks do you want to add?");
                int tankAdd;
                tankAdd = input.nextInt();
                intstock[4] = intstock [4] + tankAdd;
            }
        
        }else if (s.equals("2")){
        
        System.out.println("Please choose item category to remove");
            System.out.println("1)Base\n2)Battery\n3)Coil\n4)Liquid\n5)Tank");
            int stockResponse;
            stockResponse = input.nextInt();
            if(stockResponse == 1){
                System.out.println("How many bases do you want to remove?");
                int baseAdd;
                baseAdd = input.nextInt();
                intstock[0] = intstock[0] - baseAdd;
            }else if(stockResponse == 2){
                System.out.println("How many batteries do you want to remove?");
                int battAdd;
                battAdd = input.nextInt();
                intstock[1] = intstock[1] - battAdd;
            }else if (stockResponse == 3){
                System.out.println("How many coils do you want to remove?");
                int coilAdd;
                coilAdd = input.nextInt();
                intstock[2] = intstock [2] - coilAdd;
            }else if (stockResponse == 4){
                System.out.println("How many liquids do you want to remove?");
                int liquidAdd;
                liquidAdd = input.nextInt();
                intstock[3] = intstock [3] - liquidAdd;
            }else if (stockResponse == 5){
                System.out.println("How many tanks do you want to remove?");
                int tankAdd;
                tankAdd = input.nextInt();
                intstock[4] = intstock [4] - tankAdd;
            }
        
        
        }else if (s.equals("3")){
            
           for(int dab = 0;dab < intstock.length; dab++){
             
             
             int intVal = intstock[dab];
             String StringVal = stringstock[dab];
             
             System.out.println("There are "+intVal+" "+StringVal+" 's in stock");
               
        
        }
        
        }
    
    } else if (s.equals("5")) {} else if (s.equals("6")) {
     String itemName = "";
     double itemPrice = 0;
     int fourtwentyblazeit = 0;
     do {

      int saleResponse;
      String itemType;
      int responseSwag;
      System.out.println("Which item would you like to add?");
      System.out.println("1)Base\n2)Battery\n3)Coil\n4)Liquid\n5)Tank");
      saleResponse = input.nextInt();
      if (saleResponse == 1){
          
          
          System.out.println("Please select base form");
          itemType = "Base";
          System.out.println("1)Descreet\n2)Slim\n3)Small\n4)Medium\n5)Large");
          int formResponse;
          formResponse = input.nextInt();
          
          if (formResponse == 1){
              itemForm = eForm.DESCREET;
          }else if (formResponse == 2){
              itemForm = eForm.SLIM;
          }if (formResponse == 3){
              itemForm = eForm.MEDIUM;
          }if (formResponse == 4){
              itemForm = eForm.LARGE;
          }
          
          System.out.println("Please select item color");
          System.out.println("1)White\n2)Black\n3)Blue\n4)Red\n5)Green\n6)Purple\n7)Cyan\n8)Yellow");
          int colorResponse;
          
          colorResponse = input.nextInt();
          
          if (colorResponse == 1){
              itemColor = eColor.WHITE;
          }else if (colorResponse == 2){
              itemColor = eColor.BLACK;
          }else if (colorResponse == 3){
              itemColor = eColor.BLUE;
          }else if (colorResponse == 4){
              itemColor = eColor.RED;
          }else if (colorResponse == 5){
              itemColor = eColor.PURPLE;
          }else if (colorResponse == 6){
              itemColor = eColor.CYAN;
          }else if (colorResponse == 7){
              itemColor = eColor.YELLOW;
          }
          
          System.out.println("Please enter a price for the selected item");
          itemCost = input.nextDouble();
          System.out.println("How many of the item would you like?");
          itemAmount = input.nextInt();
          itemMath = itemCost*itemAmount;
          totalAmount = totalAmount + itemMath;
          items.add(new Base(itemForm, itemColor));
          
      }else if (saleResponse == 2){
         
          System.out.println("Please select battery form");
          itemType = "Base";
          System.out.println("1)Descreet\n2)Slim\n3)Small\n4)Medium\n5)Large");
          int formResponse;
          formResponse = input.nextInt();
          
          if (formResponse == 1){
              itemForm = eForm.DESCREET;
          }else if (formResponse == 2){
              itemForm = eForm.SLIM;
          }if (formResponse == 3){
              itemForm = eForm.MEDIUM;
          }if (formResponse == 4){
              itemForm = eForm.LARGE;
          }
          
          System.out.println("Please enter battery voltage");
          double voltage;
          voltage = input.nextDouble();
          
          
          System.out.println("Please enter a price for the selected item");
          itemCost = input.nextDouble();
          System.out.println("How many of the item would you like?");
          itemAmount = input.nextInt();
          itemMath = itemCost*itemAmount;
          totalAmount = totalAmount + itemMath;
          items.add(new Battery(itemForm, voltage));
      }else if(saleResponse ==3){
          
          System.out.println("Please select coil wire");
          System.out.println("1)Kathanal32\n2)Nichrome32\n3)Steel22\n4)Nickel24\n5)Titanium32");
          int formResponse;
          formResponse = input.nextInt();
          
          if (formResponse == 1){
              itemWire = eWire.KANTHAL32;
          }else if (formResponse == 2){
              itemWire = eWire.NICHROME32;
          }if (formResponse == 3){
              itemWire = eWire.STEEL22;
          }if (formResponse == 4){
              itemWire = eWire.TITANIUM32;
          }
        
          System.out.println("Please enter a price for the selected item");
          itemCost = input.nextDouble();
          System.out.println("How many of the item would you like?");
          itemAmount = input.nextInt();
          itemMath = itemCost*itemAmount;
          totalAmount = totalAmount + itemMath;
          items.add(new Coil(itemWire));
      }else if(saleResponse ==4){
          System.out.println("Please enter name of flavor");
          String flavor;
          flavor = input.next();
          System.out.println("Please enter amount of nicotine in the falvor");
          double nicotine;
          nicotine = input.nextDouble();
          
          System.out.println("Please enter a price for the selected item");
          itemCost = input.nextDouble();
          System.out.println("How many of the item would you like?");
          itemAmount = input.nextInt();
          itemMath = itemCost*itemAmount;
          totalAmount = totalAmount + itemMath;
          items.add(new Liquid(flavor,nicotine));
      }else if(saleResponse ==5){
          System.out.println("Please enter the capacity of the tank");
          double capacity;
          capacity = input.nextDouble();
          System.out.println("Select the form of the tank");
          System.out.println("1)Descreet\n2)Slim\n3)Small\n4)Medium\n5)Large");
          int formResponse;
          formResponse = input.nextInt();
          
          if (formResponse == 1){
              itemForm = eForm.DESCREET;
          }else if (formResponse == 2){
              itemForm = eForm.SLIM;
          }if (formResponse == 3){
              itemForm = eForm.MEDIUM;
          }if (formResponse == 4){
              itemForm = eForm.LARGE;
          }
          
          System.out.println("Please enter a price for the selected item");
          itemCost = input.nextDouble();
          System.out.println("How many of the item would you like?");
          itemAmount = input.nextInt();
          itemMath = itemCost*itemAmount;
          totalAmount = totalAmount + itemMath;
          items.add(new Battery(itemForm,capacity));
          
      }
      
      

      System.out.println("Would you like to add another item?\n1)Yes\n2)No");
      responseSwag = input.nextInt();

      if (responseSwag == 1) {
       fourtwentyblazeit = 0;
      } else if (responseSwag == 2) {
       fourtwentyblazeit = 1;
      }

     } while (fourtwentyblazeit == 0);
     System.out.println("Displaying receipt");
     Sale newSale = new Sale(items, user, totalAmount);
     System.out.println(newSale);
    } else if (s.equals("7")) {
     logout();
    }
   } else if (user instanceof Admin) {
    genMenu(user);
    String s = input.next();
    // Else not needed, menu will show if it breaks
    if (s.equals("1")) {
     if (s.equals("1")) {
      System.out.println("\n\n1) Remove account");
      System.out.println("2) Change name");
      System.out.println("3) Change ID");
      System.out.println("4) Add account");
      System.out.println("5) To main menu");
      s = input.next();
      if (s.equals("1")) {
       try {
        System.out.println("User ID: ");
        String id = input.next();
        modEmployee("r", id, "", "", "");
       } catch (Exception e) {
        System.out.println(e.toString());
       }
      }
      if (s.equals("2")) {
       System.out.println("User ID: ");
       String id = input.next();
       System.out.println("\n\nFirstname:");
       String fname = input.next();
       System.out.println("Surname:");
       String sname = input.next();
       try {
        modEmployee("cn", id, fname, sname, id);
       } catch (Exception e) {
        System.out.println(e.toString());
       }
      }
      if (s.equals("3")) {
       System.out.println("User ID: ");
       String id = input.next();
       System.out.println("New ID: ");
       String nid = input.next();
       try {
        modEmployee("ci", id, "", "", nid);
       } catch (Exception e) {
        System.out.println(e.toString());
       }
      }
      if (s.equals("4")) {
       System.out.println("Firstname:");
       String fname = input.next();
       System.out.println("Lastname:");
       String lname = input.next();
       System.out.println("ID:");
       int id = input.nextInt();
       System.out.println("Password:");
       String pass = input.next();
       admins.add(new Admin(fname, lname, id, pass));
      }
     }
    } else if (s.equals("2")) {System.out.println("1) Add Stock");
        System.out.println("2) Remove Stock");
        System.out.println("3) List Stock");
        s = input.next();
        if (s.equals("1")){
            System.out.println("Please choose item category to add");
            System.out.println("1)Base\n2)Battery\n3)Coil\n4)Liquid\n5)Tank");
            int stockResponse;
            stockResponse = input.nextInt();
            if(stockResponse == 1){
                System.out.println("How many bases do you want to add?");
                int baseAdd;
                baseAdd = input.nextInt();
                intstock[0] = intstock[0] + baseAdd;
            }else if(stockResponse == 2){
                System.out.println("How many batteries do you want to add?");
                int battAdd;
                battAdd = input.nextInt();
                intstock[1] = intstock[1] + battAdd;
            }else if (stockResponse == 3){
                System.out.println("How many coils do you want to add?");
                int coilAdd;
                coilAdd = input.nextInt();
                intstock[2] = intstock [2] + coilAdd;
            }else if (stockResponse == 4){
                System.out.println("How many liquids do you want to add?");
                int liquidAdd;
                liquidAdd = input.nextInt();
                intstock[3] = intstock [3] + liquidAdd;
            }else if (stockResponse == 5){
                System.out.println("How many tanks do you want to add?");
                int tankAdd;
                tankAdd = input.nextInt();
                intstock[4] = intstock [4] + tankAdd;
            }
        
        }else if (s.equals("2")){
        
        System.out.println("Please choose item category to remove");
            System.out.println("1)Base\n2)Battery\n3)Coil\n4)Liquid\n5)Tank");
            int stockResponse;
            stockResponse = input.nextInt();
            if(stockResponse == 1){
                System.out.println("How many bases do you want to remove?");
                int baseAdd;
                baseAdd = input.nextInt();
                intstock[0] = intstock[0] - baseAdd;
            }else if(stockResponse == 2){
                System.out.println("How many batteries do you want to remove?");
                int battAdd;
                battAdd = input.nextInt();
                intstock[1] = intstock[1] - battAdd;
            }else if (stockResponse == 3){
                System.out.println("How many coils do you want to remove?");
                int coilAdd;
                coilAdd = input.nextInt();
                intstock[2] = intstock [2] - coilAdd;
            }else if (stockResponse == 4){
                System.out.println("How many liquids do you want to remove?");
                int liquidAdd;
                liquidAdd = input.nextInt();
                intstock[3] = intstock [3] - liquidAdd;
            }else if (stockResponse == 5){
                System.out.println("How many tanks do you want to remove?");
                int tankAdd;
                tankAdd = input.nextInt();
                intstock[4] = intstock [4] - tankAdd;
            }
        
        
        }else if (s.equals("3")){
            
           for(int dab = 0;dab < intstock.length; dab++){
             
             
             int intVal = intstock[dab];
             String StringVal = stringstock[dab];
             
             System.out.println("There are "+intVal+" "+StringVal+" 's in stock");
               
        
        }
        
        } 
    } else if (s.equals("3")) {} else if (s.equals("4")) {
     String itemName = "";
     double itemPrice = 0;
     int fourtwentyblazeit = 0;
     do {

         
      int saleResponse;
      String itemType;
      int responseSwag;
      System.out.println("Which item would you like to add?");
      System.out.println("1)Base\n2)Battery\n3)Coil\n4)Liquid\n5)Tank");
      saleResponse = input.nextInt();
      if (saleResponse == 1){
          
          
          System.out.println("Please select base form");
          itemType = "Base";
          System.out.println("1)Descreet\n2)Slim\n3)Small\n4)Medium\n5)Large");
          int formResponse;
          formResponse = input.nextInt();
          
          if (formResponse == 1){
              itemForm = eForm.DESCREET;
          }else if (formResponse == 2){
              itemForm = eForm.SLIM;
          }if (formResponse == 3){
              itemForm = eForm.MEDIUM;
          }if (formResponse == 4){
              itemForm = eForm.LARGE;
          }
          
          System.out.println("Please select item color");
          System.out.println("1)White\n2)Black\n3)Blue\n4)Red\n5)Green\n6)Purple\n7)Cyan\n8)Yellow");
          int colorResponse;
          
          colorResponse = input.nextInt();
          
          if (colorResponse == 1){
              itemColor = eColor.WHITE;
          }else if (colorResponse == 2){
              itemColor = eColor.BLACK;
          }else if (colorResponse == 3){
              itemColor = eColor.BLUE;
          }else if (colorResponse == 4){
              itemColor = eColor.RED;
          }else if (colorResponse == 5){
              itemColor = eColor.PURPLE;
          }else if (colorResponse == 6){
              itemColor = eColor.CYAN;
          }else if (colorResponse == 7){
              itemColor = eColor.YELLOW;
          }
          
          System.out.println("Please enter a price for the selected item");
          itemCost = input.nextDouble();
          System.out.println("How many of the item would you like?");
          itemAmount = input.nextInt();
          itemMath = itemCost*itemAmount;
          totalAmount = totalAmount + itemMath;
          items.add(new Base(itemForm, itemColor));
          
      }else if (saleResponse == 2){
         
          System.out.println("Please select battery form");
          itemType = "Base";
          System.out.println("1)Descreet\n2)Slim\n3)Small\n4)Medium\n5)Large");
          int formResponse;
          formResponse = input.nextInt();
          
          if (formResponse == 1){
              itemForm = eForm.DESCREET;
          }else if (formResponse == 2){
              itemForm = eForm.SLIM;
          }if (formResponse == 3){
              itemForm = eForm.MEDIUM;
          }if (formResponse == 4){
              itemForm = eForm.LARGE;
          }
          
          System.out.println("Please enter battery voltage");
          double voltage;
          voltage = input.nextDouble();
          
          
          System.out.println("Please enter a price for the selected item");
          itemCost = input.nextDouble();
          System.out.println("How many of the item would you like?");
          itemAmount = input.nextInt();
          itemMath = itemCost*itemAmount;
          totalAmount = totalAmount + itemMath;
          items.add(new Battery(itemForm, voltage));
      }else if(saleResponse ==3){
          
          System.out.println("Please select coil wire");
          System.out.println("1)Kathanal32\n2)Nichrome32\n3)Steel22\n4)Nickel24\n5)Titanium32");
          int formResponse;
          formResponse = input.nextInt();
          
          if (formResponse == 1){
              itemWire = eWire.KANTHAL32;
          }else if (formResponse == 2){
              itemWire = eWire.NICHROME32;
          }if (formResponse == 3){
              itemWire = eWire.STEEL22;
          }if (formResponse == 4){
              itemWire = eWire.TITANIUM32;
          }
        
          System.out.println("Please enter a price for the selected item");
          itemCost = input.nextDouble();
          System.out.println("How many of the item would you like?");
          itemAmount = input.nextInt();
          itemMath = itemCost*itemAmount;
          totalAmount = totalAmount + itemMath;
          items.add(new Coil(itemWire));
      }else if(saleResponse ==4){
          System.out.println("Please enter name of flavor");
          String flavor;
          flavor = input.next();
          System.out.println("Please enter amount of nicotine in the falvor");
          double nicotine;
          nicotine = input.nextDouble();
          
          System.out.println("Please enter a price for the selected item");
          itemCost = input.nextDouble();
          System.out.println("How many of the item would you like?");
          itemAmount = input.nextInt();
          itemMath = itemCost*itemAmount;
          totalAmount = totalAmount + itemMath;
          items.add(new Liquid(flavor,nicotine));
      }else if(saleResponse ==5){
          System.out.println("Please enter the capacity of the tank");
          double capacity;
          capacity = input.nextDouble();
          System.out.println("Select the form of the tank");
          System.out.println("1)Descreet\n2)Slim\n3)Small\n4)Medium\n5)Large");
          int formResponse;
          formResponse = input.nextInt();
          
          if (formResponse == 1){
              itemForm = eForm.DESCREET;
          }else if (formResponse == 2){
              itemForm = eForm.SLIM;
          }if (formResponse == 3){
              itemForm = eForm.MEDIUM;
          }if (formResponse == 4){
              itemForm = eForm.LARGE;
          }
          
          System.out.println("Please enter a price for the selected item");
          itemCost = input.nextDouble();
          System.out.println("How many of the item would you like?");
          itemAmount = input.nextInt();
          itemMath = itemCost*itemAmount;
          totalAmount = totalAmount + itemMath;
          items.add(new Battery(itemForm,capacity));
          
      }
      
      

      System.out.println("Would you like to add another item?\n1)Yes\n2)No");
      responseSwag = input.nextInt();

      if (responseSwag == 1) {
       fourtwentyblazeit = 0;
      } else if (responseSwag == 2) {
       fourtwentyblazeit = 1;
      }

     } while (fourtwentyblazeit == 0);
     System.out.println("Displaying receipt");
     Sale newSale = new Sale(items, user, totalAmount);
     System.out.println(newSale);
    } else if (s.equals("5")) {
     logout();
    }
   } else if (user instanceof Employee) {
    genMenu(user);
    String s = input.next();
    if (s.equals("1")) {System.out.println("1) Add Stock");
        System.out.println("2) Remove Stock");
        System.out.println("3) List Stock");
        s = input.next();
        if (s.equals("1")){
            System.out.println("Please choose item category to add");
            System.out.println("1)Base\n2)Battery\n3)Coil\n4)Liquid\n5)Tank");
            int stockResponse;
            stockResponse = input.nextInt();
            if(stockResponse == 1){
                System.out.println("How many bases do you want to add?");
                int baseAdd;
                baseAdd = input.nextInt();
                intstock[0] = intstock[0] + baseAdd;
            }else if(stockResponse == 2){
                System.out.println("How many batteries do you want to add?");
                int battAdd;
                battAdd = input.nextInt();
                intstock[1] = intstock[1] + battAdd;
            }else if (stockResponse == 3){
                System.out.println("How many coils do you want to add?");
                int coilAdd;
                coilAdd = input.nextInt();
                intstock[2] = intstock [2] + coilAdd;
            }else if (stockResponse == 4){
                System.out.println("How many liquids do you want to add?");
                int liquidAdd;
                liquidAdd = input.nextInt();
                intstock[3] = intstock [3] + liquidAdd;
            }else if (stockResponse == 5){
                System.out.println("How many tanks do you want to add?");
                int tankAdd;
                tankAdd = input.nextInt();
                intstock[4] = intstock [4] + tankAdd;
            }
        
        }else if (s.equals("2")){
        
        System.out.println("Please choose item category to remove");
            System.out.println("1)Base\n2)Battery\n3)Coil\n4)Liquid\n5)Tank");
            int stockResponse;
            stockResponse = input.nextInt();
            if(stockResponse == 1){
                System.out.println("How many bases do you want to remove?");
                int baseAdd;
                baseAdd = input.nextInt();
                intstock[0] = intstock[0] - baseAdd;
            }else if(stockResponse == 2){
                System.out.println("How many batteries do you want to remove?");
                int battAdd;
                battAdd = input.nextInt();
                intstock[1] = intstock[1] - battAdd;
            }else if (stockResponse == 3){
                System.out.println("How many coils do you want to remove?");
                int coilAdd;
                coilAdd = input.nextInt();
                intstock[2] = intstock [2] - coilAdd;
            }else if (stockResponse == 4){
                System.out.println("How many liquids do you want to remove?");
                int liquidAdd;
                liquidAdd = input.nextInt();
                intstock[3] = intstock [3] - liquidAdd;
            }else if (stockResponse == 5){
                System.out.println("How many tanks do you want to remove?");
                int tankAdd;
                tankAdd = input.nextInt();
                intstock[4] = intstock [4] - tankAdd;
            }
        
        
        }else if (s.equals("3")){
            
           for(int dab = 0;dab < intstock.length; dab++){
             
             
             int intVal = intstock[dab];
             String StringVal = stringstock[dab];
             
             System.out.println("There are "+intVal+" "+StringVal+" 's in stock");
               
        
        }
        
        } else if (s.equals("2")) {} else if (s.equals("3")) {
            
     String itemName = "";
     double itemPrice = 0;
     int fourtwentyblazeit = 0;
     do {

      int saleResponse;
      String itemType;
      int responseSwag;
      System.out.println("Which item would you like to add?");
      System.out.println("1)Base\n2)Battery\n3)Coil\n4)Liquid\n5)Tank");
      saleResponse = input.nextInt();
      if (saleResponse == 1){
          
          
          System.out.println("Please select base form");
          itemType = "Base";
          System.out.println("1)Descreet\n2)Slim\n3)Small\n4)Medium\n5)Large");
          int formResponse;
          formResponse = input.nextInt();
          
          if (formResponse == 1){
              itemForm = eForm.DESCREET;
          }else if (formResponse == 2){
              itemForm = eForm.SLIM;
          }if (formResponse == 3){
              itemForm = eForm.MEDIUM;
          }if (formResponse == 4){
              itemForm = eForm.LARGE;
          }
          
          System.out.println("Please select item color");
          System.out.println("1)White\n2)Black\n3)Blue\n4)Red\n5)Green\n6)Purple\n7)Cyan\n8)Yellow");
          int colorResponse;
          
          colorResponse = input.nextInt();
          
          if (colorResponse == 1){
              itemColor = eColor.WHITE;
          }else if (colorResponse == 2){
              itemColor = eColor.BLACK;
          }else if (colorResponse == 3){
              itemColor = eColor.BLUE;
          }else if (colorResponse == 4){
              itemColor = eColor.RED;
          }else if (colorResponse == 5){
              itemColor = eColor.PURPLE;
          }else if (colorResponse == 6){
              itemColor = eColor.CYAN;
          }else if (colorResponse == 7){
              itemColor = eColor.YELLOW;
          }
          
          System.out.println("Please enter a price for the selected item");
          itemCost = input.nextDouble();
          System.out.println("How many of the item would you like?");
          itemAmount = input.nextInt();
          itemMath = itemCost*itemAmount;
          totalAmount = totalAmount + itemMath;
          items.add(new Base(itemForm, itemColor));
          
      }else if (saleResponse == 2){
         
          System.out.println("Please select battery form");
          itemType = "Base";
          System.out.println("1)Descreet\n2)Slim\n3)Small\n4)Medium\n5)Large");
          int formResponse;
          formResponse = input.nextInt();
          
          if (formResponse == 1){
              itemForm = eForm.DESCREET;
          }else if (formResponse == 2){
              itemForm = eForm.SLIM;
          }if (formResponse == 3){
              itemForm = eForm.MEDIUM;
          }if (formResponse == 4){
              itemForm = eForm.LARGE;
          }
          
          System.out.println("Please enter battery voltage");
          double voltage;
          voltage = input.nextDouble();
          
          
          System.out.println("Please enter a price for the selected item");
          itemCost = input.nextDouble();
          System.out.println("How many of the item would you like?");
          itemAmount = input.nextInt();
          itemMath = itemCost*itemAmount;
          totalAmount = totalAmount + itemMath;
          items.add(new Battery(itemForm, voltage));
      }else if(saleResponse ==3){
          
          System.out.println("Please select coil wire");
          System.out.println("1)Kathanal32\n2)Nichrome32\n3)Steel22\n4)Nickel24\n5)Titanium32");
          int formResponse;
          formResponse = input.nextInt();
          
          if (formResponse == 1){
              itemWire = eWire.KANTHAL32;
          }else if (formResponse == 2){
              itemWire = eWire.NICHROME32;
          }if (formResponse == 3){
              itemWire = eWire.STEEL22;
          }if (formResponse == 4){
              itemWire = eWire.TITANIUM32;
          }
        
          System.out.println("Please enter a price for the selected item");
          itemCost = input.nextDouble();
          System.out.println("How many of the item would you like?");
          itemAmount = input.nextInt();
          itemMath = itemCost*itemAmount;
          totalAmount = totalAmount + itemMath;
          items.add(new Coil(itemWire));
      }else if(saleResponse ==4){
          System.out.println("Please enter name of flavor");
          String flavor;
          flavor = input.next();
          System.out.println("Please enter amount of nicotine in the falvor");
          double nicotine;
          nicotine = input.nextDouble();
          
          System.out.println("Please enter a price for the selected item");
          itemCost = input.nextDouble();
          System.out.println("How many of the item would you like?");
          itemAmount = input.nextInt();
          itemMath = itemCost*itemAmount;
          totalAmount = totalAmount + itemMath;
          items.add(new Liquid(flavor,nicotine));
      }else if(saleResponse ==5){
          System.out.println("Please enter the capacity of the tank");
          double capacity;
          capacity = input.nextDouble();
          System.out.println("Select the form of the tank");
          System.out.println("1)Descreet\n2)Slim\n3)Small\n4)Medium\n5)Large");
          int formResponse;
          formResponse = input.nextInt();
          
          if (formResponse == 1){
              itemForm = eForm.DESCREET;
          }else if (formResponse == 2){
              itemForm = eForm.SLIM;
          }if (formResponse == 3){
              itemForm = eForm.MEDIUM;
          }if (formResponse == 4){
              itemForm = eForm.LARGE;
          }
          
          System.out.println("Please enter a price for the selected item");
          itemCost = input.nextDouble();
          System.out.println("How many of the item would you like?");
          itemAmount = input.nextInt();
          itemMath = itemCost*itemAmount;
          totalAmount = totalAmount + itemMath;
          items.add(new Battery(itemForm,capacity));
          
      }
      
      

      System.out.println("Would you like to add another item?\n1)Yes\n2)No");
      responseSwag = input.nextInt();

      if (responseSwag == 1) {
       fourtwentyblazeit = 0;
      } else if (responseSwag == 2) {
       fourtwentyblazeit = 1;
      }

     } while (fourtwentyblazeit == 0);
     System.out.println("Displaying receipt");
     Sale newSale = new Sale(items, user, totalAmount);
     System.out.println(newSale);
    } else if (s.equals("4")) {
     logout();
    }
    // Else not needed, menu will show if it breaks

   } else {
    genMenu(user);
     s = input.next();
    if (s.equals("1")) {
     System.exit(0);
    }
   }
  }
   // prove that we can throw exceptionss
   try{
  menu();
   throw new EndOfMenuException();} catch(Exception e) {
      
  } 
 }}

 public static void modOwner(String mod, String id, String changefName, String changesName, String changeID) throws NoSuchPersonException {
  // Find Owner to modify
  Person toMod = new Employee();
  int j = 0;
  while (j < owners.size()) {
   if (owners.get(j).getID().equals(id)) {
    toMod = owners.get(j);
    // Remove person
    if (mod.equals("r")) {
     owners.remove(toMod);
    }
    // Change name
    if (mod.equals("cn")) {
     toMod.setFirstname(changefName);
     toMod.setLastname(changesName);
    }
    // Change id
    if (mod.equals("ci")) {
     toMod.setID(changeID);
    }
    // Add user
    if (mod.equals("a")) {
     toMod.setID(id);
     toMod.setFirstname(changefName);
     toMod.setLastname(changesName);
     toMod.setID(changeID);
    }
   }
   if (j == owners.size()) {
    throw new NoSuchPersonException();

   }
   j++;
  }
 }

 public static void modAdmin(String mod, String id, String changefName, String changesName, String changeID) throws NoSuchPersonException {
  // Find Owner to modify
  Person toMod = new Employee();
  int j = 0;
  while (j < admins.size()) {
   if (admins.get(j).getID().equals(id)) {
    toMod = admins.get(j);
    // Remove person
    if (mod.equals("r")) {
     admins.remove(toMod);
    }
    // Change name
    if (mod.equals("cn")) {
     toMod.setFirstname(changefName);
     toMod.setLastname(changesName);
    }
    // Change id
    if (mod.equals("ci")) {
     toMod.setID(changeID);
    }
    // Add user
    if (mod.equals("a")) {
     toMod.setID(id);
     toMod.setFirstname(changefName);
     toMod.setLastname(changesName);
     toMod.setID(changeID);
    }
   }
   if (j == admins.size()) {
    throw new NoSuchPersonException();
   }
   j++;
  }
 }

 public static void modEmployee(String mod, String id, String changefName, String changesName, String changeID) throws NoSuchPersonException {
  // Find Owner to modify
  Person toMod = new Employee();
  int j = 0;
  while (j < employees.size()) {
   if (employees.get(j).getID().equals(id)) {
    toMod = employees.get(j);
    // Remove person
    if (mod.equals("r")) {
     employees.remove(toMod);
    }
    // Change name
    if (mod.equals("cn")) {
     toMod.setFirstname(changefName);
     toMod.setLastname(changesName);
    }
    // Change id
    if (mod.equals("ci")) {
     toMod.setID(changeID);
    }
    // Add user
    if (mod.equals("a")) {
     toMod.setID(id);
     toMod.setFirstname(changefName);
     toMod.setLastname(changesName);
     toMod.setID(changeID);
    }
   }
   if (j == employees.size()) {
    throw new NoSuchPersonException();
   }
   j++;
  }
 }



 public static void sell(ItemList < Item > i) {
     double cost = 0;
  Sale newSale = new Sale(i, user,cost);
 }

 public static void login(String type, String ID, String pass) {
  if (type.toLowerCase().equals("owner")) {
   // Search for info here
   int j = 0;
   while (j < owners.size()) {
    if (owners.get(j).getID().equals(ID)) {
     if (owners.get(j).authenticate(pass)) {
      user = owners.get(j);
     }
    }
   }
  }
  if (type.toLowerCase().equals("admin")) {
   // Search for info here
   int j = 0;
   while (j < admins.size()) {
    if (admins.get(j).getID().equals(ID)) {
     if (admins.get(j).authenticate(pass)) {
      user = admins.get(j);
     }
    }
   }
  }
  if (type.toLowerCase().equals("employee")) {
   // Search for info here
   int j = 0;
   while (j < employees.size()) {
    if (employees.get(j).getID().equals(ID)) {
     if (employees.get(j).authenticate(pass)) {
      user = employees.get(j);
     }
    }
   }
  }
 }

 public static void genMenu(Person user) {
  if (user instanceof Owner) {
   System.out.println("\n\n1) Add or remove owner");
   System.out.println("2) Add or remove admin");
   System.out.println("3) Add or remove employee");
   System.out.println("4) Manage stock");
   System.out.println("5) Change store info");
   System.out.println("6) Make sale");
   System.out.println("7) Log out");
  } else if (user instanceof Admin) {
   System.out.println("\n\n1) Add or remove employee");
   System.out.println("2) Manage stock");
   System.out.println("3) Change store info");
   System.out.println("4) Make sale");
   System.out.println("5) Log out");
  } else if (user instanceof Employee) {
   System.out.println("\n\n1) Manage stock");
   System.out.println("2) Change store info");
   System.out.println("3) Make sale");
   System.out.println("4) Log out");
  } else {
   System.out.println("\n\n1) Log out");
  }

 }

 public static void logout() {
  user = null;
 }
 
 // Old code goes here
 
  /**
  * @param args the command line arguments
  */
 /*public static void oldmain(String[] args) {
     Scanner input = new Scanner(System.in);
     System.out.println("Cloud9 POS System");
     System.out.println("Copyright Cloud9 inc. 2018\n\n");
     // Make the account for the owner and log them in
     user = makeGenericAccount("owner");

    static Person user;
    static String storename ="Cloud9";
    static String motto = "The best in smoke.";
            //ArrayLists for different user accounts
        // This is an example of aggregation
        static ArrayList<Employee> owners = new ArrayList<Employee>();
        static ArrayList<Employee> admins = new ArrayList<Employee>();
        static ArrayList<Employee> employees = new ArrayList<Employee>();
        
        //ArrayList for item list
        static ItemList<Item> items = new ItemList<Item>();
    /**
     * @param args the command line arguments
     */
 /*public static void oldmain(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Cloud9 POS System");
        System.out.println("Copyright Cloud9 inc. 2018\n\n");
        // Make the account for the owner and log them in
        user = makeGenericAccount("owner");
       
        //ArrayLists for different user accounts
        // This is an example of aggregation
        ArrayList<Employee> owners = new ArrayList<Employee>();
        ArrayList<Employee> admins = new ArrayList<Employee>();
        ArrayList<Employee> employees = new ArrayList<Employee>();
        
        //ArrayList for item list
        ItemList<Item> items = new ItemList<Item>();
        
        
        // Make menu here
            // The menu's options should depend on the kind of user (admin or not)
            // Infinite loop wraps around it so it doesn't crash
        
            
        // Default interaction as owner to create new users
        int option;
        String name;
        String surname;
        int ID;
        String password;
        int i = 0;
        int fourtwentyblazeit = 0;
        String itemName;
        double itemPrice;
        
        System.out.println("Welcome to Cloud9 POS System:\nPlease select an option\n1)Create new Admin\n2)Create new employee");
        option = input.nextInt();
        if(option == 1){
            do {
            System.out.println("Please enter Admins name:");
            name = input.next();
            System.out.println("Please enter "+ name + "'s surname:");
            surname = input.next();
            System.out.println("Please enter "+ name + "'s ID:");
            ID = input.nextInt();
            System.out.println("Please enter a password for " + name +"");
            password = input.next();
            admins.add(new Admin(name,surname,ID,password));
            //admins.add(new Admin(name, surname, ID, password));
            //Used to restart loop for more users
            System.out.println("Would you like to add another user?\n1)Yes\n2)No");
            int res;
            res = input.nextInt();
            if(res == 1){
                i = 0;
            }else if(res == 2){
                i = 1;
            }
        }while(i == 0);
        }    
            
        int num;
        int id;
        String pass;
        System.out.println("Welcome to Cloud9 POS System \nPlease select user type:");
        System.out.println("1)Admin\n2)Employee\n3)Owner");
        num = input.nextInt();
        if(num == 1){
            System.out.println("Please enter Admin ID:");
            id = input.nextInt();
            System.out.println("Please enter Password:");
            pass = input.next();
            int y = 0;
            while(y<admins.size()) {
                if (admins.get(y).getID().equals(id)) {
                    if (admins.get(y).authenticate(pass)) {
                        user = admins.get(y);
                    }
                }
                
                y++;
            }
            
        }else if(num == 2){
            System.out.println("Please enter Employee ID:");
            id = input.nextInt();
            System.out.println("Please enter Password:");
            pass = input.next();
             int y = 0;
            while(y<employees.size()) {
                if (employees.get(y).getID().equals(id)) {
                    if (employees.get(y).authenticate(pass)) {
                        user = employees.get(y);
                        
                        do{
                        
                        
                        int responseSwag;
                        System.out.println("Which item would you like to add?");
                        System.out.println("1)Base\n2)Battery\n3)Coil\n4)Liquid\n5)Tank\n6)Vape");
                        itemName = input.next();
                        System.out.println("Please enter item price");
                        itemPrice = input.nextInt();
                        items.add(new Item(itemName,itemPrice));
                        
                        System.out.println("Would you like to add another item?\n1)Yes\n2)No");
                        responseSwag = input.nextInt();
                        
                        if(responseSwag == 1){
                            fourtwentyblazeit = 0;
                        }else if (responseSwag == 2){
                            fourtwentyblazeit = 1;
                        }
                        
                        }while(fourtwentyblazeit == 0);
                        System.out.println("Displaying receipt");
                        Sale newSale = new Sale(items,user);
                    }
                }
                
                y++;
            }
            
        }else if(num == 3){
            System.out.println("Please enter ID:");
            id = input.nextInt();
            System.out.println("Please enter Password:");
            pass = input.next();
             int y = 0;
            while(y<admins.size()) {
                if (owners.get(y).getID().equals(id)) {
                    if (owners.get(y).authenticate(pass)) {
                        user = owners.get(y);
                    }
                }
                
                y++;
            }
        }
                
        
    }
    
     //ArrayLists for different user accounts
     // This is an example of aggregation
     ArrayList<Employee> owners = new ArrayList<Employee>();
     ArrayList<Employee> admins = new ArrayList<Employee>();
     ArrayList<Employee> employees = new ArrayList<Employee>();
     
     //ArrayList for item list
     ItemList<Item> items = new ItemList<Item>();
     
     
     // Make menu here
         // The menu's options should depend on the kind of user (admin or not)
         // Infinite loop wraps around it so it doesn't crash
     
         
     // Default interaction as owner to create new users
     int option;
     String name;
     String surname;
     int ID;
     String password;
     int i = 0;
     int fourtwentyblazeit = 0;
     String itemName;
     double itemPrice;
     
     System.out.println("Welcome to Cloud9 POS System:\nPlease select an option\n1)Create new Admin\n2)Create new employee");
     option = input.nextInt();
     if(option == 1){
         do {
         System.out.println("Please enter Admins name:");
         name = input.next();
         System.out.println("Please enter "+ name + "'s surname:");
         surname = input.next();
         System.out.println("Please enter "+ name + "'s ID:");
         ID = input.nextInt();
         System.out.println("Please enter a password for " + name +"");
         password = input.next();
         admins.add(new Admin(name,surname,ID,password));
         //admins.add(new Admin(name, surname, ID, password));
         //Used to restart loop for more users
         System.out.println("Would you like to add another user?\n1)Yes\n2)No");
         int res;
         res = input.nextInt();
         if(res == 1){
             i = 0;
         }else if(res == 2){
             i = 1;
         }
     }while(i == 0);
     }    
         
     int num;
     int id;
     String pass;
     System.out.println("Welcome to Cloud9 POS System \nPlease select user type:");
     System.out.println("1)Admin\n2)Employee\n3)Owner");
     num = input.nextInt();
     if(num == 1){
         System.out.println("Please enter Admin ID:");
         id = input.nextInt();
         System.out.println("Please enter Password:");
         pass = input.next();
         int y = 0;
         while(y<admins.size()) {
             if (admins.get(y).getID().equals(id)) {
                 if (admins.get(y).authenticate(pass)) {
                     user = admins.get(y);
                 }
             }
             
             y++;
         }
         
     }else if(num == 2){
         System.out.println("Please enter Employee ID:");
         id = input.nextInt();
         System.out.println("Please enter Password:");
         pass = input.next();
          int y = 0;
         while(y<employees.size()) {
             if (employees.get(y).getID().equals(id)) {
                 if (employees.get(y).authenticate(pass)) {
                     user = employees.get(y);
                     
                     do{
                     
                     
                     int responseSwag;
                     System.out.println("Please enter item name");
                     itemName = input.next();
                     System.out.println("Please enter item price");
                     itemPrice = input.nextInt();
                     items.add(new Item(itemName,itemPrice));
                     
                     System.out.println("Would you like to add another item?\n1)Yes\n2)No");
                     responseSwag = input.nextInt();
                     
                     if(responseSwag == 1){
                         fourtwentyblazeit = 0;
                     }else if (responseSwag == 2){
                         fourtwentyblazeit = 1;
                     }
                     
                     }while(fourtwentyblazeit == 0);
                     System.out.println("Displaying receipt");
                     Sale newSale = new Sale(items,user);
                 }
             }
             
             y++;
         }
         
     }else if(num == 3){
         System.out.println("Please enter ID:");
         id = input.nextInt();
         System.out.println("Please enter Password:");
         pass = input.next();
          int y = 0;
         while(y<admins.size()) {
             if (owners.get(y).getID().equals(id)) {
                 if (owners.get(y).authenticate(pass)) {
                     user = owners.get(y);
                 }
             }
             
             y++;
         }
     }
             
     
 }*/
 /*    
     This is kinda useless
     public static void changeMyName(String firstname, String lastname) {
         user.setFirstname(firstname);
         user.setLastname(lastname);
     }
     
     public static void changeName(Person p, String firstname, String lastname) {
         p.setFirstname(firstname);
         p.setLastname(lastname);
     }
 */
}