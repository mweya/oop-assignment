package cloud9;

/*
 * Copyright 2018 mweya.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 *
 * @author mweya
 */
import cloud9.ItemList;
import java.time.Instant;
import merch.Item;
import people.Person;
public class Receipt {
    private ItemList<Item> i;
    private Person p;
    private double cost;
    public Receipt(){
        
    }
    public Receipt(Person teller, ItemList ilist,double cost) {
        this.p = teller;
        this.i = ilist;
        this.cost = cost;
    }
    @Override
    public String toString(){
        String out = "Cloud9 Inc.\n\n";
        out = out+i.toString()+"\n";
        out = out+"Total cost N$: "+cost+"\n";
        out = out+"You've been served by:\n"+p.toString()+"\n";
        out = out+Instant.now();
        out = out+"Thanks for shopping with us, come again soon!";
        return out;
    }
}
