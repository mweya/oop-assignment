/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cloud9;

/**
 *
 * @author mweya
 */
// This is the generic class.
// It acts as a wrapper for the ArrayList class and only accepts objects of type Item.
import java.util.ArrayList;
public class ItemList<Item> {
    // Make a new array list that only accepts objects of type Item
    ArrayList<Item> list = new ArrayList<Item>();
    // Empty constructor that does nothing
    public ItemList() {}
    // If used with an item in the constructor, add the item to the list
    public ItemList(Item i) {
        list.add(i);
    }
    // If an arraylist is passed to the class, make the ItemList equal to it
    public ItemList(ArrayList<Item> i) {
        list = i;
    }
    // This adds an item to the list
    public void add(Item i) {
        list.add(i);
    }
    // This removes all elements in the list
    public void clear() {
        list.clear();
    }
    
    public String get(int i) {
        return list.get(i).toString();
    }
    
    public int size() {
        return list.size();
    }
    
    @Override
    public String toString(){
        // For the toString, loop through all the items stored
        // and return them as a string
        String out = "";
        int j = 0;
        while (j<list.size()) {
            out = out+list.get(j)+"\n";
            j++;
        }
        return out;
    }
}
