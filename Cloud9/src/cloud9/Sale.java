/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cloud9;


import enums.eColor;
import enums.eForm;
import people.Person;

/**
 *
 * @author Jono
 */
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.IOException;
public class Sale {
    
    private Person person;
    private double cashGiven;
    private double cost;
    
    private eForm form;
    private eColor color;
    
    // Might be a good idea to keep a customer's points and the
    // amount of points you'd like to use separate
    private int points;
    private int pointsUsed;
    //private String items;
    // This generic array might be better
    private ItemList i = new ItemList();

    
    public Sale(eForm form,eColor color,double cost){
        this.form = form;
        this.color = color;
        this.cost = cost;
    }
    
  public Sale(ItemList i,Person person,double cost){
        this.i = i;
        this.person = person;
        this.cost = cost;
            Receipt r = new Receipt(person,i,cost);
            System.out.println(r);
    // Write receipt to file
    try{
    PrintWriter pw = new PrintWriter("Receipt.txt");
    pw.print(r.toString());
}
    catch (Exception e){
        System.err.println("cant write to file");
    }
    }
    

}

