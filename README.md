#  OOP Assignment

## Tasks:
*  Main Menu
   *  ~~Add Manage Stock option~~
      *  List all stock
      *  Add stock
      *  (Optional) remove stock
   *  ~~Add Make sale option~~
      *  Add option to cancel transaction
      *  Use Sale class to make the sale
      *  Remove item when sold
      *  Calculate change

## Marks:
*  ~~Aggregation                              **3**~~
*  ~~Polymorphism                             **5**~~
*  Driver Class Logic                       **10**
*  ~~Comments                                 **5**~~
*  Readability, Use of Conventions          **5**
*  Creativity, going extra mile             **5**
*  ~~5 classes                                **5**~~
*  ~~Encapsulation                            **3**~~
*  ~~Composition                              **4**~~
*  ~~Inheritance                              **5**~~
*  ~~Abstract Class                           **5**~~
*  ~~2 Interfaces                             **5**~~
*  ~~Generic Class                            **5**~~
*  ~~Exception Class                          **5**~~

## User Interface
*  Login menu (first boot logs the owner in so the owner can set everything up (add accounts for employees, admins and manage stock) after which an option should be there for the owner to log out)
*  Menu should respond to account type (employees shouldn't be able to add or delete accounts, only the owner can remove or add admins)
*  The generic ItemList class (cloud9.ItemList) should print the receipt (to a textfile)
*  The receipt should be generated using the toString method of the receipt class

## Classes:
```
*  Driver Class(Could probably name it Cloud9 or Shop or even Driver)
*  Sales(The class of which we will create a new instance or object (Sales sale1 = new Sale;))
	This should generate a receipt
*  Services(Also a class with instances or objects(Services service1 = new Services;))
	(Probably need an ENUM for the different services(Repair, coil replacement, liquid refill etc. Different prices assigned to each service ENUM))
*  Generic item class
*  Vapes(pre built vapes) (extends item)
*  VapeFlavors(different juices) (extends item)
*  Coils(different coils) (extends item)
*  Batteries(different battery models) (extends item)
*  Tanks(different vape liquid tanks) (extends item)
*  'VapeMods(Many mod categories, we should just choose one like Box mods or tempreture control mods)  (extends item)'
*  RewardsSystem(Gonna need to decide how customers make points for the rewards/discount system by buying from the shop)
*  ExceptionsClasses(Will probably be more than one class. Like on for entering negative numbers as money given or RewardsException maybe?)
*  Prizes(If a customer has a certain amount of Reward points they can get a free prize, should probably have a class for all the different prizes with their details)
*  '~~Stock(available items in store)~~ I do not think a class would be a good fit for this situation'
*  People(gonna need sub classes for Admin,Employee,Customers)
*  Admin(Can create or delete other accounts)
*  Employee(Can view stock, do transactions)
*  Customers(Customer details such as names etc. As well as current reward points available)

Things I am unsure about: The customers have to give a customer satisfaction response between 1-10, dont know if we need a seperate class, probablu just add those details to the sale class as
variable. <- This is correct

Also the Employee should be able to see the amount of cash in the till, so maybe we need a Till class? The value of the till will change according to the sales made? No idea.
Thats about it I think, make changes or add/remove anything we dont need. <- Yep
```
